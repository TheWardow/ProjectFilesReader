/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projectfilesreader;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author William
 */
public class Reader {

    private BufferedReader bR;

    public Reader(String fileName) throws FileNotFoundException {
        bR = new BufferedReader(new FileReader(fileName));
    }

    public void printAllFile() throws IOException {
        String s = bR.readLine();
        while (s != null) {
            System.out.println(s);
            s = bR.readLine();
        }
    }

    public FileData fileToData() throws Exception {
        FileData fD = new FileData();
        int line = 0;
        String s = bR.readLine();

        while (s != null) {
            if (!s.startsWith("#")) {
                line++;

                String[] splited = s.split(" ");
                if (line == 1) {
                    processFirstLine(fD, splited);
                    if (splited.length != 3) {
                        throw new Exception("La premiere ligne du fichier est mal formatée");
                    }
                } else if (line <= 1 + fD.getNbPoints()) {
                    processPoint(fD, splited);
                } else if (line <= 1 + fD.getNbPoints() + fD.getNbSegments()) {
                    processSegment(fD, splited);
                } else if (line <= 1 + fD.getNbPoints() + fD.getNbSegments() + fD.getNbFaces()) {
                    processFace(fD, splited);
                }
            }
            s = bR.readLine();
        }
        return fD;
    }

    private void processFirstLine(FileData fD, String[] splited) throws Exception {
        if (splited.length != 3) {
            throw new Exception("La premiere ligne du fichier est mal formatée");
        } else {
            int p, s, f;
            p = Integer.parseInt(splited[0]);
            s = Integer.parseInt(splited[1]);
            f = Integer.parseInt(splited[2]);
            fD.setValues(p, s, f);
        }

    }

    private void processPoint(FileData fD, String[] splited) {
        
    }

    private void processSegment(FileData fD, String[] splited) {

    }

    private void processFace(FileData fD, String[] splited) {

    }
}
